/*
 * Decompiled with CFR 0.139.
 * 
 * Could not load the following classes:
 *  com.palmergames.bukkit.towny.object.Nation
 *  com.palmergames.bukkit.towny.object.Resident
 *  com.palmergames.bukkit.towny.object.Town
 *  com.palmergames.bukkit.towny.object.TownyUniverse
 *  me.clip.placeholderapi.expansion.PlaceholderExpansion
 *  org.bukkit.Bukkit
 *  org.bukkit.entity.Player
 *  org.bukkit.plugin.Plugin
 */
package com.extendedclip.papi.expansion.towny;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.palmergames.bukkit.towny.object.TownyUniverse;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;

public class TownyExpansion extends PlaceholderExpansion {
	public boolean canRegister() {
		return Bukkit.getPluginManager().getPlugin(this.getPlugin()) != null;
	}

	public String getAuthor() {
		return "clip";
	}

	public String getIdentifier() {
		return "towny";
	}

	public String getPlugin() {
		return "Towny";
	}

	public String getVersion() {
		return "1.0.0";
	}

	/*
	 * Enabled force condition propagation Lifted jumps to return sites
	 */
	public String onPlaceholderRequest(Player p, String identifier) {
		if (p == null) {
			return "";
		}
		String string = identifier;
		switch (string.hashCode()) {
		case -1852993317: {
			if (string.equals("surname"))
				return this.getPlayersSurname(p);
			return null;
		}
		case -1848678983: {
			if (string.equals("town_rank"))
				return this.getTownRank(p);
			return null;
		}
		case -1848641138: {
			if (string.equals("town_size"))
				return this.getTownSize(p);
			return null;
		}
		case -1479080421: {
			if (string.equals("town_mayor"))
				return this.getTownMayor(p);
			return null;
		}
		case -1052618937: {
			if (string.equals("nation"))
				return this.getPlayersNation(p);
			return null;
		}
		case -947139025: {
			if (string.equals("town_balance"))
				return this.getTownBankBalance(p);
			return null;
		}
		case -752369555: {
			if (string.equals("town_tag"))
				return this.getTownTag(p);
			return null;
		}
		case -600094315: {
			if (string.equals("friends"))
				return this.getPlayersFriends(p);
			return null;
		}
		case 3566226: {
			if (string.equals("town"))
				return this.getPlayersTown(p);
			return null;
		}
		case 91171364: {
			if (string.equals("nation_rank"))
				return this.getNationRank(p);
			return null;
		}
		case 110371416: {
			if (string.equals("title"))
				return this.getPlayersTownyTitle(p);
			return null;
		}
		case 1996698550: {
			if (string.equals("town_residents"))
				return this.getTownResidents(p);
			return null;
		}
		}
		return null;
	}

	private String getPlayersTown(Player p) {
		String town = "";
		try {
			town = "�8[�b" + String.valueOf(TownyUniverse.getDataSource().getResident(p.getName()).getTown().getName())
					+ "�8]�r ";
		} catch (Exception exception) {
			// empty catch block
		}
		return town;
	}

	private String getPlayersFriends(Player p) {
		String res = "";
		try {
			res = String.valueOf(TownyUniverse.getDataSource().getResident(p.getName()).getFriends().size());
		} catch (Exception e) {
			return "";
		}
		return res;
	}

	private String getPlayersNation(Player p) {
		String nation = "";
		try {
			nation = String
					.valueOf(TownyUniverse.getDataSource().getResident(p.getName()).getTown().getNation().getName());
		} catch (Exception e) {
			return "";
		}
		return nation;
	}

	private String getPlayersTownyTitle(Player p) {
		String title = "";
		try {
			title = String.valueOf(TownyUniverse.getDataSource().getResident(p.getName()).getTitle());
		} catch (Exception e) {
			return "";
		}
		return title;
	}

	private String getPlayersSurname(Player p) {
		String title = "";
		try {
			title = String.valueOf(TownyUniverse.getDataSource().getResident(p.getName()).getSurname());
		} catch (Exception e) {
			return "";
		}
		return title;
	}

	private String getTownResidents(Player p) {
		String res = "";
		try {
			res = String.valueOf(TownyUniverse.getDataSource().getResident(p.getName()).getTown().getNumResidents());
		} catch (Exception e) {
			return "";
		}
		return res;
	}

	private String getTownBankBalance(Player p) {
		String bal = "";
		try {
			bal = String.valueOf(
					formatMoney(TownyUniverse.getDataSource().getResident(p.getName()).getTown().getHoldingBalance()));
		} catch (Exception e) {
			return "";
		}
		return bal;
	}

	private String getTownMayor(Player p) {
		String mayor = "";
		try {
			mayor = String
					.valueOf(TownyUniverse.getDataSource().getResident(p.getName()).getTown().getMayor().getName());
		} catch (Exception e) {
			return "";
		}
		return mayor;
	}

	private String getTownRank(Player p) {
		String rank;
		block3: {
			rank = "";
			try {
				List<String> ranks = TownyUniverse.getDataSource().getResident(p.getName()).getTownRanks();
				if (ranks == null || ranks.isEmpty())
					break block3;
				for (String r : ranks) {
					if (!TownyUniverse.getDataSource().getResident(p.getName()).hasTownRank(r))
						continue;
					rank = r;
					break;
				}
			} catch (Exception e) {
				return "";
			}
		}
		return rank;
	}

	private String getNationRank(Player p) {
		String rank;
		block3: {
			rank = "";
			try {
				List<String> ranks = TownyUniverse.getDataSource().getResident(p.getName()).getNationRanks();
				if (ranks == null || ranks.isEmpty())
					break block3;
				for (String r : ranks) {
					if (!TownyUniverse.getDataSource().getResident(p.getName()).hasNationRank(r))
						continue;
					rank = r;
					break;
				}
			} catch (Exception e) {
				return "";
			}
		}
		return rank;
	}

	private String getTownSize(Player p) {
		String size = "";
		try {
			size = String.valueOf(TownyUniverse.getDataSource().getResident(p.getName()).getTown().getTotalBlocks());
		} catch (Exception e) {
			return "";
		}
		return size;
	}

	private String getTownTag(Player p) {
		String tag = "";
		try {
			tag = String.valueOf(TownyUniverse.getDataSource().getResident(p.getName()).getTown().getTag());
		} catch (Exception e) {
			return "";
		}
		return tag;
	}

	private String formatMoney(double money) {
		String output = NumberFormat.getNumberInstance(Locale.US).format(money);
		return output;
	}
}
